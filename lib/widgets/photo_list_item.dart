import 'package:devsteamtest/photo.dart';
import 'package:devsteamtest/screens/view_photo.dart';
import 'package:flutter/material.dart';

class PhotoListItem extends StatefulWidget {

  final Photo data;

  PhotoListItem(this.data);

  @override
  State<StatefulWidget> createState() => _PhotoListItemState();
}

class _PhotoListItemState extends State<PhotoListItem> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.pushNamed(
          context,
          ViewPhoto.routeName,
          arguments: widget.data
        );
      },
      child: Card(
        child: ListTile(
          leading: AspectRatio(
            aspectRatio: 1,
            child: ClipOval(
              child: Image.network(
                widget.data.smallUrl,
                fit: BoxFit.fill,
                loadingBuilder: (context, child, loadingProgress) {
                  if (loadingProgress == null)
                    return child;
                  return Container(
                    color: Colors.grey,
                    child: Icon(Icons.camera_alt,),
                  );
                },
              ),
            ),
          ),
          isThreeLine: false,
          title: Text(widget.data.name),
          subtitle: Text(widget.data.author),
        ),
      ),
    );
  }

}