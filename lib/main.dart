import 'package:devsteamtest/network/network_provider.dart';
import 'package:devsteamtest/photo.dart';
import 'package:devsteamtest/screens/photo_list.dart';
import 'package:devsteamtest/screens/view_photo.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() {
  runApp(MyApp());
}

// Я не придумал как вставить в данное приложение все патерны, технологии из
// задания, но впринципе я знаком почти со всеми.
// Ранее не использовал Drawer и TabController и связанные с ним.

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    //Не придумал как еще можно использовать тут shared preferences
    SharedPreferences.setMockInitialValues({
      'ACCESS_KEY': 'cf49c08b444ff4cb9e4d126b7e9f7513ba1ee58de7906e4360afc1a33d1bf4c0'
    });
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.grey,
        primaryColorDark: Colors.black,
      ),
      initialRoute: PhotoList.routeName,
      onGenerateRoute: _onGenerateRoute,
    );
  }
  Route _onGenerateRoute(RouteSettings settings) {
    var page;
    print(settings);
    switch (settings.name) {
      case PhotoList.routeName:
        page = PhotoList();
        break;
      case ViewPhoto.routeName:
        page = ViewPhoto(photo: settings.arguments as Photo,);
    }
    assert(page != null, 'unknown route name');
    return PageRouteBuilder(
      pageBuilder: (context, animation, secondaryAnimation) => page,
      transitionsBuilder: (context, animation, secondaryAnimation, child) {
        var begin = Offset(1.0, 0.0);
        var end = Offset.zero;
        var curve = Curves.ease;
        var tween = Tween(begin: begin, end: end).chain(CurveTween(curve: curve));
        return SlideTransition(
          position: animation.drive(tween),
          child: child,
        );
      },
    );
  }
}