import 'dart:convert';
import 'dart:typed_data';
import 'package:devsteamtest/photo.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;


class NetworkProvider {

  static final _instance = NetworkProvider._();

  NetworkProvider._();

  static NetworkProvider getInstance() => _instance;

  static const ACCESS_KEY = 'ACCESS_KEY';
  static const URL = 'api.unsplash.com';
  static const PHOTOS = '/photos';

  Future<Map<String, String>> get _headers async {
    var prefs = await SharedPreferences.getInstance();
    String key = prefs.getString(ACCESS_KEY);
    var headers = {
      'Authorization': 'Client-ID $key',
      'Accept-Version': 'v1',
    };
    return headers;
  }

  Future<List<dynamic>> getPhotos(int page) async {
    var body = {
      'page': '$page',
      'per_page':'20'
    };
    var uri = Uri.https(URL, PHOTOS, body);
    var response = await http.get(uri, headers: await _headers);
    if(response.statusCode == 200) {
      return jsonDecode(response.body);
    }
    else {
      print('Failed to load photos');
    }
  }

}