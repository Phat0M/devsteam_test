import 'dart:convert';

class Photo {

  final String name;
  final String author;
  final String fullUrl;
  final String smallUrl;

  Photo({
    this.name, this.author, this.fullUrl, this.smallUrl
  })
      : assert(name != null),
        assert(author != null),
        assert(fullUrl != null),
        assert(smallUrl != null);

  factory Photo.fromJson(Map<String, dynamic> json) {
    print(json);
    print(json['alt_description']);
    print((json['user'])['name']);
    print((json['urls'])['full']);
    print(json['urls']['small']);
    return Photo(
      name: json['alt_description'] ?? json['description'] ?? 'no name',
      author: (json['user'])['name'],
      fullUrl: (json['urls'])['full'],
      smallUrl: (json['urls'])['small'],
    );
  }

  @override
  String toString() {
    return 'Photo{name: $name, author: $author, fullUrl: $fullUrl, smallUrl: $smallUrl}';
  }


}