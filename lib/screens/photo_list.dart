import 'package:devsteamtest/network/network_provider.dart';
import 'package:devsteamtest/photo.dart';
import 'package:devsteamtest/widgets/photo_list_item.dart';
import 'package:flutter/material.dart';

import 'view_photo.dart';

class PhotoList extends StatefulWidget{

  static const routeName = "/";

  @override
  State<StatefulWidget> createState() => _PhotoListState();
}

class _PhotoListState extends State<PhotoList> {

  var _page = 0;
  NetworkProvider provider;
  List<Photo> photos = [];

  @override
  void initState() {
    super.initState();
    provider = NetworkProvider.getInstance();
    addPhotos();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Text("Photos from unsplash"),
      ),
      body: Container(
        child: RefreshIndicator(
          child: ListView.builder(
            padding: EdgeInsets.all(10),
            itemCount: photos.length,
            itemBuilder: _itemBuilder,
          ),
          onRefresh: () async {
            // Это чтобы пользователь понял, что что-то обновилось
            setState(() {
              photos.clear();
            });
            _page = 0;
            addPhotos();
            return null;
          },
        ),
      ),
    );
  }

  Widget _itemBuilder(BuildContext context, int index) {
    if(index >= photos.length - 1) {
      addPhotos();
      return Container(
        height: 75,
        child: Center(
          child: CircularProgressIndicator(),
        ),
      );
    }
    print(photos[index]);
    return PhotoListItem(photos[index]);
  }

  Future addPhotos() async {
    var data = await _download();
    setState(() {
      photos.addAll(data);
    });
  }

  Future<List<Photo>> _download() async {
    var data = await provider.getPhotos(_page++);
    List<Photo> photos = [];
    data.forEach((element) {
      setState(() {
        photos.add(Photo.fromJson(element));
      });
    });
    return photos;
  }
}