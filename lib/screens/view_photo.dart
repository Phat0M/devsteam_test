import 'dart:typed_data';

import 'package:devsteamtest/photo.dart';
import 'package:flutter/material.dart';

class ViewPhoto extends StatefulWidget {

  static const routeName = '/viewphoto';
  final Photo photo;

  ViewPhoto({
    this.photo
  })
      :assert(photo != null);

  @override
  State<StatefulWidget> createState() => _ViewPhotoState();
}

class _ViewPhotoState extends State<ViewPhoto> {

  @override
  Widget build(BuildContext context) {
    var photo = Image.network(widget.photo.smallUrl, fit:  BoxFit.cover,);
    return Theme(
        data: ThemeData(
            primaryColor: Colors.black
        ),
        child: Scaffold(
            appBar: AppBar(
              title: Text(widget.photo.name),
            ),
            body: Container(
              decoration: BoxDecoration(
                color: Colors.black,
              ),
              child: Center(
                  child: Image.network(
                    widget.photo.fullUrl,
                    fit:  BoxFit.cover,
                    loadingBuilder: (context, child, loadingProgress) {
                      if (loadingProgress == null)
                        return child;
                      return Stack(
                          children: [
                            Center(child: photo),
                            Center(
                              child: CircularProgressIndicator(
                                valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
                                value: loadingProgress.expectedTotalBytes != null
                                    ? loadingProgress.cumulativeBytesLoaded / loadingProgress.expectedTotalBytes
                                    : null,
                              ),
                            )
                          ]
                      );
                    },
                  )
              ),
            )
        )
    );
  }
}

//class ViewPhotoArguments